<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/coba', function () {
//     return view('_layouts.coba');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/landing', function() {
//   return view('landing');
// })->name('landing');

// Route::get('/', 'MagangController@landing')->name('welcome'); // untuk ke welcome
// Route::get('/tugas/{variabel}/{data}', 'MagangController@tugas')->name('tugas'); // untuk ke view tugas

// Untuk Route CR
Route::get('/', 'CrudController@dashboard');
Route::get('/dashboardmhs', 'CrudController@dashboardmhs')->name('mahasiswa.dashboardmhs');
Route::get('/usermhs', 'CrudController@usermhs')->name('mahasiswa.usermhs');
Route::get('/mhspinjam', 'CrudController@mhspinjam')->name('mahasiswa.mhspinjam');
Route::get('/mhskembali', 'CrudController@mhskembali')->name('mahasiswa.mhskembali');
Route::get('/mhsinput', 'CrudController@mhsinput')->name('mahasiswa.mhsinput');
Route::get('/mhsupdate', 'CrudController@mhsupdate')->name('mahasiswa.mhsupdate');
Route::get('/mhscheck', 'CrudController@mhscheck')->name('mahasiswa.mhscheck');

//akademik---------------------------------------------------------------------------------
Route::get('/akauser', 'CrudController@akauser')->name('akademik.akauser');
Route::get('/akadashboard', 'CrudController@akadashboard')->name('akademik.akadashboard');
Route::get('/akabebas', 'CrudController@akabebas')->name('akademik.akabebas');
Route::get('/akastatus', 'CrudController@akadidalam')->name('akademik.didalam');

//kepala lab--------------------------------------------------------------------------------
Route::get('/labuser', 'CrudController@labuser')->name('lab.labuser');
Route::get('/labdashboard', 'CrudController@labdashboard')->name('lab.labdashboard');
Route::get('/labkonvirmasi', 'CrudController@labkonvirmasi')->name('lab.labkonvirmasi');
Route::get('/labbebas', 'CrudController@labbebas')->name('lab.labbebas');
Route::get('/tambah-iventaris', 'CrudController@labtambah')->name('lab.labtambah');
Route::get('/list', 'CrudController@mhslist')->name('lab.list');

//dashboard awal-----------------------------------------------------------------------------
Route::get('/labolatorium', 'CrudController@labolatorium')->name('dashboardawal.labolatorium');
Route::get('/pelayanan', 'CrudController@pelayanan')->name('dashboardawal.pelayanan');
Route::get('/kepalalab', 'CrudController@kepalalab')->name('dashboardawal.kepalalab');

Route::get('/help', 'CrudController@help')->name('crud.help');

Route::get('/homelogin', 'CrudController@homelogin')->name('crud.homelogin');
Route::get('/akademik', 'CrudController@akademik')->name('crud.akademik');
Route::get('/kepala', 'CrudController@kepala')->name('crud.kepala');
Route::get('/mahasiswa', 'CrudController@mahasiswa')->name('crud.mahasiswa');

Route::get('/dashboard', 'CrudController@dashboard')->name('crud.dashboard');
Route::get('/crud/create', 'CrudController@create')->name('crud.create');
Route::post('/crud/store', 'CrudController@store')->name('crud.store');
// Route::get('/crud/home', 'CrudController@home')->name('_layouts.home');

// Untuk Route UD
Route::get('/crud/edit/{crud}', 'CrudController@edit')->name('crud.edit');
Route::put('/crud/update/{crud}', 'CrudController@update')->name('crud.update');
Route::delete('/crud/destroy/{crud}', 'CrudController@destroy')->name('crud.destroy');
