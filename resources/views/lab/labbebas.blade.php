@extends('_layouts.induklab')
@section('lab')
<div class="space-y-2">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Bebas Labolatorium
        </p>
    </div>
    <div class="flex px-2 py-2 bg-yellow-400 justify-left items-left text-left">
        <div class="flex-initial px-2">
          <img class="h-5/12 w-8" src="/img/iconpdf.png" alt="">
        </div>
        <div class="flex-initial self-center">
        <a class="font-normal text-base text-black">Tamplate Surat Keterangan Konvirmasi Labolatorium</a>
        </div>
        <div class="flex-initial self-center px-4">
            <a href=""class="font-normal text-base text-white bg-blue-900 px-4 py-1 rounded-full hover:bg-blue-800">Unduh</a>
        </div>
      </div>
      <div class="flex w-full bg-yellow-300 py-4 px-4 rounded-lg">
        <div class="flex justify-start text-center w-full">
            <p class="text-xl text-gray-800">Simpan Data</p>
        </div>
        <div class="flex justify-end text-center w-full">
            <a href="#" class="text-right px-6 py-2 rounded-full bg-blue-900 hover:bg-blue-700 text-white">Simpan</a>
        </div>
    </div>
    <div>
        <p class="text-center text-2xl">Konvirmasi Bebas Lab</p>
        <table class="rounded-lg w-full mt-2">
              <tr class="bg-gray-100">
                <th class="px-2 bg-yellow-400">No</th>
                <th class="px-2 bg-yellow-400">Nama Pengaju</th>
                <th class="px-2 bg-yellow-400">NIM</th>
                <th class="px-2 bg-yellow-400">Program Studi</th>
                <th class="px-2 bg-yellow-400">Semester</th>
                <th class="px-2 bg-yellow-400">Upload Bukti Surat Keterangan Konvirmasi Bebas Lab</th>
                <th class="px-2 bg-yellow-400">Konvirmasi Bebas Lab</th>      
                <th class="px-2 bg-yellow-400">Status</th>             
              </tr>
              <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="px-2">1</td>
                <td class="px-2">Sintiany Dewi Ratna Sari</td>
                <td class="px-2">1709075026</td>
                <td class="px-2">T. Elektro</td>
                <td class="px-2">7</td>
                <td class="px-2">
                    <input type="file" name="filename" class="py-3">
                </td>
                <td class="px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-8 py-2">Konvirmasi</a>                    
                </td>      
                <td class="px-2">Belum Terkonvirmasi</td>             
              </tr>
          </table>       
    </div>
</div>
@endsection


