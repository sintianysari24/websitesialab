{{-- File yg mau digunakan sebagai parent --}}
@extends('_layouts.tambah')
{{-- lokasi untuk menaruh posisi file ini pada file parent --}}
@section('tambah')
<h2 class="font-medium text-xl text-center">FORM PENGISIAN DATA IVENTARIS</h2><br>
  <div class="bg-white p-10 rounded-lg">
    <form >
    {{-- action="{{route('crud.store')}}" method="POST" class=""> --}}
      @csrf
      <div class="space-y-8 mb-10">
        <div class="space-y-2">
          <label for="namaAlat" class="capitalize">No Alat</label>
          <input type="text" name="noAlat" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
        </div>

        <div class="space-y-2">
          <label for="namaAlat" class="capitalize">Nama Alat</label>
          <input type="text" name="namaAlat" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
        </div>

        <div class="space-y-2">
          <label for="jumlah" class="capitalize">Jumlah</label>
          <input type="text" name="jumlah" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">      
        </div>

        <div class="space-y-2">
          <label for="jumlahPeminjam" class="capitalize">Jumlah Pinjam</label>
          <input type="text" name="jumlahPinjam" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">      
        </div>

        <div class="space-y-2">
          <label for="kondisi" class="capitalize">Kondisi</label>
          <input type="text" name="kondisi" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">       
        </div>

        <div class="space-y-2">
          <label for="tahunPembelian" class="capitalize">Tahun Pembelian</label>
          <input type="text" name="tahunPembelian" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">       
        </div>

      </div>
      <div class="bg-yellow-500  -mr-10 -ml-10 -mb-10 px-10 py-5 item-center text-right" style="border-radius: 0 0 0.5rem 0.5rem;">
        <button type="submit" class="bg-blue-900 px-4 py-2 rounded-lg hover:opacity-50 text-white">
          Simpan
        </button>
      </div>
    </form>
  </div>
@endsection
