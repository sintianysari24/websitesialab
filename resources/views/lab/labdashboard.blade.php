@extends('_layouts.induklab')
@section('lab')
                        {{-- <script>
                            function sisa(){
                                interval = setInterval("sisa2()",1);
                            }
                            function sisa2(){
                                one = document.autoSumForm.jumlah.value;
                                two = document.autoSumForm.jumlahPinjam.value;
                                document.autoSumForm.tersedia.value = (one * 1) - (two * 1);
                            }
                            function stopSisa2(){
                                clearInterval(interval);
                            }
                        </script> --}}
<div class="space-y-2">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Data Iventaris Alat Labolatorium
        </p>
    </div>
    <div class="flex flex-col w-full bg-yellow-300 py-3 rounded-lg">
        <div class="flex">
            <p class="w-full text-2xl px-5 py-1">
                Tambah Data Alat Inventaris
            </p>
        </div>
        <div class="flex ">
            <a href="{{route('crud.create')}}" class="flex bg-blue-900 px-4 py-1 hover:bg-blue-700 text-white text-xl m-4 rounded-full">Tambah Data</a>
        </div>
    </div>
    <div>
        <table class="rounded-lg w-full mt-2">
            <thead class="bg-white">
              <tr class="bg-gray-100">
                <th class="border-b px-2 bg-yellow-400">No</th>
                <th class="border-b px-2 bg-yellow-400">No Alat</th>
                <th class="border-b px-2 bg-yellow-400">Nama Alat</th>
                <th class="border-b px-2 bg-yellow-400">Jumlah</th>
                <th class="border-b px-2 bg-yellow-400">Jumlah Pinjam</th>
                <th class="border-b px-2 bg-yellow-400">Data Nama Peminjam</th>
                <th class="border-b px-2 bg-yellow-400">Tersedia</th>
                <th class="border-b px-2 bg-yellow-400">Kondisi</th>
                <th class="border-b px-2 bg-yellow-400">Tahun Pembelian</th>      
                <th class="border-b px-2 bg-yellow-400">Aksi</th>             
              </tr>
            <thead>
            <tbody class="bg-white">
                {{-- @foreach ($sialab as $index => $item)
                <tr class="hover:bg-yellow-200 border-b">
                    <td class="text-center px-2 py-2">
                      {{ $index + 1 }}
                    </td>
                    <td class="text-center px-2 py-2" >
                      {{ $item->noAlat }}
                    </td>
                    <td class="text-center px-2 py-2" >
                      {{ $item->namaAlat }}
                    </td>
                    <td class="text-center px-2 py-2"  onfocus="sisa1" onblur="sisa2">
                      {{ $item->jumlah }}
                    </td>
                    <td class="text-center px-2 py-2"  onfocus="sisa1" onblur="sisa2">
                        {{ $item->jumlahPinjam }}
                    </td>
                    <td class="text-center px-2 py-2">
                        <a href="{{route('lab.list')}}" class="px-16 py-1 rounded-full bg-blue-900 hover:bg-blue-700 text-white m-2">Lihat</a>
                    </td>
                    <td class="text-center px-2 py-2" onfocus="sisa1" onblur="sisa2">

                    </td>
                    <td class="text-center px-2 py-2">
                      {{ $item->kondisi }}
                    </td>
                    <td class="text-center px-2 py-2">
                        {{ $item->tahunPembelian }}
                    </td>
                    <td class="text-center px-2 py-2">
                        <div class="flex space-x-3 justify-center">
                            <div class="flex">
                                <a href="{{route('crud.edit', $item->id) }}" class="material-icons text-gray-700 hover:text-green-600">edit</a>
                            </div>
                            <div>
                                <form action="{{ route('crud.destroy', $item->id)}}"method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button  type="submit" class="material-icons text-gray-700 hover:text-red-600">delete</button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr> --}}
                {{-- @endforeach --}}
                {{-- @if ($sialab->count() == 0) --}}
                <tr class="hover:bg-gray-100">
                  <td colspan="10" class="text-center py-2">Tidak Ada Data</td>
                </tr>
                {{-- @endif --}}
            </tbody>
        </table>      
    </div>
</div>
@endsection

