@extends('_layouts.induklab')
@section('lab')
<div class="space-y-2">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Proses Konvirmasi Pengembalian Alat
        </p>
    </div>
    <div class="flex w-full bg-yellow-300 py-4 px-4 rounded-lg">
        <div class="flex justify-start text-center w-full">
            <p class="text-xl text-gray-800">Simpan Data</p>
        </div>
        <div class="flex justify-end text-center w-full">
            <a href="#" class="text-right px-6 py-2 rounded-full bg-blue-900 hover:bg-blue-700 text-white">Simpan</a>
        </div>
    </div>
    <div>
        <p class="text-center text-2xl">Konvirmasi Pengembalian Alat</p>
        <table class="rounded-lg w-full mt-2">
              <tr class="bg-gray-100">
                <th class="px-2 bg-yellow-400">No</th>
                <th class="px-2 bg-yellow-400">Nama Alat</th>
                <th class="px-2 bg-yellow-400">Nama Peminjam</th>
                <th class="px-2 bg-yellow-400">Program Studi</th>
                <th class="px-2 bg-yellow-400">Tanggal Pinjam</th>
                <th class="bg-yellow-400">Tanggal Kembali</th>
                <th class="bg-yellow-400">Download Surat</th>
                <th class="px-2 bg-yellow-400">Konvirmasi Pengembalian Alat</th>      
                <th class="px-2 bg-yellow-400">Status</th>             
              </tr>
              <tr class="bg-white hover:bg-yellow-200 text-center ">
                <td class="px-2">1</td>
                <td class="px-2">Multimeter Digital</td>
                <td class="px-2">Sintiany Dewi Ratna Sari</td>
                <td class="px-2">T.Elektro</td>
                <td class="px-2">09/10/2020</td>
                <td class="">
                    <input type="text" name="tglKembali" placeholder="dd/mm/yyyy" class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 py-1 rounded shadow leading-tight focus:outline-none focus:shadow-outline text-center ">        
                </td>
                <td class="justify-center">
                    <a href="#" class="material-icons text-gray-700 hover:text-green-600">file_download</a>
                </td>
                <td class="px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-8 py-2">Konvirmasi</a>                    
                </td>      
                <td class="px-2">Belum Kembali</td>             
              </tr>
              <tr class="bg-white hover:bg-yellow-200 text-center ">
                <td class="px-2">2</td>
                <td class="px-2">Power Supply</td>
                <td class="px-2">Sintiany Dewi Ratna Sari</td>
                <td class="px-2">T.Elektro</td>
                <td class="px-2">09/10/2020</td>
                <td class="">
                    <input type="text" name="tglKembali" placeholder="dd/mm/yyyy" class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 py-1 rounded shadow leading-tight focus:outline-none focus:shadow-outline text-center ">        
                </td>
                <td class="justify-center">
                    <a href="#" class="material-icons text-gray-700 hover:text-green-600">file_download</a>
                </td>
                <td class="px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-8 py-2">Konvirmasi</a>                    
                </td>      
                <td class="px-2">Telah Kembali</td>             
              </tr>
        </table>       
    </div>
</div>
@endsection


