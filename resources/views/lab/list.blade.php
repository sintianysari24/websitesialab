@extends('_layouts.mhslist')
@section('list')
<p class="w-full text-2xl py-1">Data Pengembalian Alat Labolatorium</p>
<div class="">
    <table class="rounded-lg w-full mt-2">
        <thead class="bg-white">
        <tr class="bg-gray-100">
            <th class="border-b px-2 bg-yellow-400">No</th>
            <th class="border-b px-2 bg-yellow-400">Nama</th>   
            <th class="border-b px-2 bg-yellow-400">NIM</th>    
            <th class="border-b px-2 bg-yellow-400">Prodi</th>       
        </tr>
        </thead>
        <tr class="bg-gray-100 hover:bg-yellow-200 text-center">
            <td class="border-b px-2">1</td>
            <td class="border-b px-2">Sintiany Dewi Ratna Sari</td>   
            <td class="border-b px-2">170907506</td>    
            <td class="border-b px-2 flex justify-center">T. Elektro</td>       
        </tr>
    </table>       
</div>
@endsection