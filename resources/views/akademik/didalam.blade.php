@extends('_layouts.dalam')
@section('dalam')
<div class="grid grid-rows-3 grid-flow-col gap-4">
    <div class="row-span-2 bg-yellow-600 px-2 py-2 rounded-lg">
        <div class="space-x-2">
            <div class="w-full px-8">
                <p class="justify-center text-xl text-white">Nama Mahasiswa </p>
                <p class="justify-center text-white">Sintiany Dewi Ratna Sari</p><br>
                <p class="justify-center text-white text-xl">NIM </p>
                <p class="justify-center text-white">1709075026</p><br>               
                <p class="justify-center text-white text-xl">tanggal pengajuan </p>
                <p class="justify-center text-white">17/10/2020</p>               
            </div>
        </div>
    </div>
    <div class="row-span-3 col-span-1 bg-white border rounded-lg border-yellow-700">
        <div class="flex flex-col space-y-2">
            <div class="row-span-1 col-span-2 bg-white rounded-lg py-4">
                <div class="text-center">
                    <div class="text-3xl m-b-md">
                    Pergerakan Surat<br>
                    </div>
                </div>
                <div class="md:flex justify-center px-16">
                    <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center space-y-2">
                        <div class="flex-1  px-16">
                            <img class="h-5/12 w-20 m-3" src="/img/berkas.png" alt="">
                        </div>
                        <div class="flex-1 px-16">
                            <p class="text-xl">Proses<br>Pegistrasi</P>
                        </div>
                        <div class="flex-1">
                            <a href="#" class="text-base bg-yellow-600 hover:bg-yellow-700 rounded-full text-white px-4 py-1">Konvirmasi</a>
                        </div>
                    </div>
                    <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center space-y-2">
                        <div class="flex-1 px-16">
                            <img class="h-5/12 w-20 m-3" src="/img/terima.png" alt="">
                        </div>
                        <div class="flex-1 px-16">
                            <p class="text-xl">Data<br>Terkirim</P>
                        </div>
                        <div class="flex-1">
                            <a href="#" class="text-base bg-yellow-600 hover:bg-yellow-700 rounded-full text-white px-4 py-1">Konvirmasi</a>
                        </div>
                    </div>
                    <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center space-y-2">
                        <div class="flex-1 px-16">
                            <img class="h-5/12 w-20 m-3" src="/img/centang.png" alt="">
                        </div>
                        <div class="flex-1 px-16">
                            <p class="text-xl">Proses<br>Pengesahan</P>
                        </div>
                        <div class="flex-1">
                            <a href="#" class="text-base bg-yellow-600 hover:bg-yellow-700 rounded-full text-white px-4 py-1">Konvirmasi</a>
                        </div>
                    </div>
                    <div class="border border-yellow-600 rounded-lg px-2 py-2 md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center space-y-2">
                        <div class="flex-1 px-24">
                            <img class="h-5/12 w-20 m-3" src="/img/upload.png" alt="">
                        </div>
                        <div class="flex-1 px-16">
                            <p class="text-xl">Upload<br>Surat</P>
                        </div>
                        <div class="flex-1">
                              <input type="file" name="filename">
                        </div>
                        <div class="space-y-2">   
                            <div class="flex-1">
                                <a href="#" class="text-base bg-yellow-600 hover:bg-yellow-700 rounded-full text-white px-4 py-1">Kirim</a>
                            </div>     
                            <div class="flex-1">
                                <a href="#" class="text-base bg-yellow-600 hover:bg-yellow-700 rounded-full text-white px-4 py-1">Konvirmasi</a>
                            </div>   
                        </div>            
                    </div>
                </div>
                <div class=" flex space-x-3 w-full justify-center m-4">
                    <div class="flex">
                        <a href="#" class="px-6 py-2 rounded-full bg-blue-900 hover:bg-blue-700 text-white">Simpan</a>
                    </div>
                    <div class="flex">
                        <a href="{{route('akademik.akabebas')}}" class="px-6 py-2 rounded-full bg-blue-900 hover:bg-blue-700 text-white">Kembali</a>
            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection