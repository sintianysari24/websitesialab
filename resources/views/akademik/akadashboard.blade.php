@extends('_layouts.indukakademik')
@section('akademik')
<div class=" w-full bg-yellow-500">
<p class="w-full text-2xl px-2 py-1 text-white font-bold">
    Pengajuan Surat Keterangan Bebas Labolatorium
</p>
</div>
<div class="">
    <p class="w-full text-xl px-2 font-bold text-center">
        <br>Data Nama Pengaju
    </p>
    <table class="rounded-lg w-full mt-4">
        <tr class="bg-gray-100">
          <th class="border-b px-2 bg-yellow-400">No</th>
          <th class="border-b px-1 bg-yellow-400">Tanggal Pengajuan</th>
          <th class="border-b px-2 bg-yellow-400">Nama Mahasiswa</th>
          <th class="border-b px-2 bg-yellow-400">NIM</th>
          <th class="border-b px-2 bg-yellow-400">Tanggal Lahir</th>
          <th class="border-b px-2 bg-yellow-400">Alamat</th>
          <th class="border-b px-2 bg-yellow-400">No. HP</th>
          <th class="border-b px-2 bg-yellow-400">Keperluan</th>      
          <th class="border-b px-2 bg-yellow-400">Bukti SPP</th>
          <th class="border-b px-2 bg-yellow-400">Surat Konvirmasi Lab</th>             
        </tr>
        <tr class="bg-white hover:bg-yellow-200 text-center">
          <td class="border-b px-2">1</td>
          <td class="border-b px-1">17/10/2020</td>
          <td class="border-b px-2">Sintiany Dewi Ratna Sari</td>
          <td class="border-b px-2">1709075026</td>
          <td class="border-b px-2">24/08/1999</td>
          <td class="border-b px-2">Jl. Lumba-Lumba</td>
          <td class="border-b px-2">085248542712</td>
          <td class="border-b px-2">SK Bebas Lab</td>      
          <td class="border-b px-2">
            <a href="#" class="rounded-lg bg-blue-900 hover:bg-blue-800 text-white text-center px-4">Unduh</a>
          </td>
          <td class="border-b px-2">
            <a href="#" class="rounded-lg bg-blue-900 hover:bg-blue-800 text-white text-center px-4">Unduh</a>
          </td>             
        </tr>
    </table>
</div>
@endsection
