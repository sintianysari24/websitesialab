@extends('_layouts.indukakademik')
@section('akademik')
<div class=" felx w-full bg-yellow-500">
    <p class="w-full text-2xl px-2 py-1 text-white font-bold">
        Data Mahasiswa Bebas Labolatorium
    </p>
</div>
<div class="">
    <p class="w-full text-xl px-2 font-bold text-center">
        <br>Data Mahasiswa Bebas Labolatorium
    </p>
    <p class="text-xs text-gray-800">
        Note: Proses validasi data terutama terkait surat bukti konvirmasi bebas labolatorium dilakukan oleh bagian akademik
    </p>
    <table class="rounded-lg w-full ">
      <thead class="bg-white">
        <tr class="bg-gray-100">
          <th class="px-2 bg-yellow-400">No</th>
          <th class="px-auto bg-yellow-400">Tanggal Pengajuan</th>
          <th class="px-auto bg-yellow-400 px-4">Nama Mahasiswa</th>
          <th class="px-auto bg-yellow-400">NIM</th>
          <th class="px-auto bg-yellow-400">Surat Bukti Konvirmasi Bebas Labolatorium</th>
          <th class="px-auto bg-yellow-400">Status Validasi Lab</th>
          <th class="px-4 bg-yellow-400">Update Pergerakan Surat</th>
          <th class="px-auto bg-yellow-400">Tanggal Selesai</th>                  
        </tr>
        <tr class="bg-white hover:bg-yellow-200 text-center">
          <td class="px-1">1</td>
          <td class="px-1">17/10/2020</td>
          <td class="px-2">Sintiany Dewi Ratna Sari</td>
          <td class="px-2">1709075026</td>
          <td class="px-2">
            <a href="#" class="py-2 rounded-full bg-blue-900 hover:bg-blue-700 text-white text-center px-20 mb-1">Unduh</a>
          </td>
          <td class="px-2 ">
            <select class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 py-1 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
              <option>Pilih Status</option>
              <option>Valid</option>
              <option>Tidak Valid</option>
            </select>
          </td>
          <td class="px-4">
            <a href="{{route('akademik.didalam')}}" class="py-2 rounded-full bg-blue-900 hover:bg-blue-700 text-white text-center px-16 mb-1">Lihat</a>
          </td>
          <td class="px-2">
            <input type="text" name="tglSelesai" placeholder="dd/mm/yyyy" class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 py-1 rounded shadow leading-tight focus:outline-none focus:shadow-outline text-center ">        
          </td>                  
        </tr>
      </thead>
    </table>
</div>
@endsection
