@extends('_layouts.induk5')
@section('induk5')

<p class="text-3xl text-center">SELAMAT DATANG, CIVITAS AKADEMIKA</p>
<p class="text-lg text-center">Harap Pilih Sesuai Dengan Status Anda</p><br>
<div class="md:flex">
    <div class="md:flex-shrink-0 mt-6 flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-red-600 px-4 py-2 m-2 rounded-lg">
            <div class="flex justify-center">
                <img class="h-5/12 w-20 m-3" src="/img/akademik.png" alt="">
            </div>
            <P class="text-3xl text-left justify-left">Akademik</P><br>
        </div>
        <div class="text-left p-2">
            <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('crud.akademik')}}">Select</a> 
        </div>
    </div>
    <div class="md:flex-shrink-0 mt-6 flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-yellow-600 px-4 py-2 m-2 rounded-lg">
            <div class="flex justify-center">
                <img class="h-5/12 w-24 m-3" src="/img/labolatorium.png" alt="">
            </div>
            <P class="text-3xl text-left justify-left">Kepala Labolatorium</P><br>
        </div>
        <div class="text-left p-2">
            <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('crud.kepala')}}">Select</a> 
        </div>
    </div>
    <div class="md:flex-shrink-0 mt-6 flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-green-600 px-4 py-2 m-2 rounded-lg">
            <div class="flex justify-center">
                <img class="h-5/12 w-24 m-3" src="/img/mahasiswa.png" alt="">
            </div>
            <P class="text-3xl text-left justify-left">Mahasiswa</P><br>
        </div>
        <div class="text-left p-2">
            <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('crud.mahasiswa')}}">Select</a> 
        </div>
    </div>
</div>
<div class="flex justify-center">
    <a  href="{{route('crud.dashboard')}}" class="m-10 font-bold py-3 px-4 rounded-full bg-blue-800 hover:bg-gray-600 text-white text-center">KEMBALI</a>  
</div>

@endsection