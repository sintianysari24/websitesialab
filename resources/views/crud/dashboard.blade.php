@extends('_layouts.induk1')
@section('induk1')  

<p class="text-3xl text-center justify-center font-bold text-white">
    Selamat Datang
</p>
<p class="text-2xl text-center justify-center text-white">
    Portal Labolatorium Fakultas Teknik <br> Universitas Mulawarman
</p>
<div>
<div class="md:flex">
    <div class="md:flex-shrink-0 mt-6 flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-orange-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-3xl text-left justify-left">Labolatorium</P><br>
            <div class="text-left p-2">
                <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('dashboardawal.labolatorium')}}">Select</a> 
            </div>
        </div>
        <div class="text-blue-900 text-center bg-white-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-base text-left text-justify">Fakultas Teknik memiliki berbagai macam fasilitas di dalamnya termasuk penyediaan labolatorium,
                 yang dapat menjadi sarana uji coba</P>
        </div>
    </div>
    <div class="md:flex-shrink-0 mt-6 flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-orange-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-3xl text-left justify-left">Pelayanan </P><br>
            <div class="text-left p-2">
                <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('dashboardawal.pelayanan')}}">Select</a> 
            </div>
        </div>
        <div class="text-blue-900 text-center bg-white-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-base text-left text-justify">Labolatorium fakultas teknik tentunya memberikan pelayanan yang berguna bagi mahasiswa sebagai contoh
                seperti peminjaman alat, guna sebagai penunjang kegiatan mahasiswa terkait penelitian
            </P>
        </div>
    </div>
    <div class="md:flex-shrink-0 mt-6 flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-orange-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-3xl text-left justify-left">Kepala Labolatorium</P><br>
            <div class="text-left p-2">
                <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('dashboardawal.kepalalab')}}">Select</a> 
            </div>
        </div>
        <div class="text-blue-900 text-center bg-white-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-base text-left text-justify">Berikut adalah data kepala labolatorium di fakultas teknik</P>
        </div>
    </div>
</div>
@endsection