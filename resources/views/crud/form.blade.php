{{-- File yg mau digunakan sebagai parent --}}
@extends('_layouts.default')
{{-- lokasi untuk menaruh posisi file ini pada file parent --}}
@section('content')
<h2 class="font-medium text-xl text-center">FORM PENGISIAN DATA UNIVERSITAS</h2><br>
  <div class="bg-white p-10 rounded-lg">
    <form action="{{route('crud.store')}}" method="POST" class="">
      @csrf
      <div class="space-y-8 mb-10">
        <div class="space-y-2">
          <label for="nama" class="capitalize">Nama Universitas </label>
          <input type="text" name="nama" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
        </div>

        <div class="space-y-2">
          <label for="alamat" class="capitalize">Alamat </label>
          <input type="text" name="alamat" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">      
        </div>

        <div class="space-y-2">
          <label for="kota" class="capitalize">Kota </label>
          <input type="text" name="kota" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">       
        </div>

        <div class="space-y-2">
          <label for="akreditasi" class="capitalize">Akreditasi </label>
          <input type="text" name="akreditasi" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
          <p class="text-red-500 text-xs italic">*Mohon di isi sesuai akreditasi universitas terbaru</p>        
        </div>

        <div class="space-y-2">
          <label for="email" class="capitalize">Email</label>
          <input type="text" name="email" placeholder="username@gmial.com" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
          <p class="text-red-500 text-xs italic">*Mohon di isi sesuai email atau laman resmi universitas</p>       
        </div>

        <div class="space-y-2">
          <label for="tahun" class="capitalize">Tahun Didirikan</label>
          <input type="text" name="tahun" placeholder="yyyy" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded-lg leading-tight focus:outline-none focus:bg-white focus:border-gray-500">        
        </div>

      </div>
      <div class="bg-yellow-400  -mr-10 -ml-10 -mb-10 px-10 py-5 item-center text-right" style="border-radius: 0 0 0.5rem 0.5rem;">
        <button type="submit" class="bg-green-700 px-4 py-2 rounded-lg hover:opacity-50 text-white">
          Simpan
        </button>
      </div>
    </form>
  </div>
@endsection
