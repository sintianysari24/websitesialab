@extends('_layouts.induk4')
@section('induk4')


<div>
  <p class="text-3xl font-bold">Help</p><br>
  <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Mahasiswa</div>
    <ul class="px-5 py-4 leading-loose list-decimal text-xl"> 
        <li class="">Mahasiswa dapat mengajukan peminjamna alat</li>
        <li class="">Mahasiswa dapat mengajukan pembuatan bebas labolatorium apabila mahasiswa yang bersangkutan telah terferivikasi di seluruh kepala labolatorium</li>
        <li class="">Mahasiswa dapat memantau perkembangan pembuatan surat bebas labolatorium di fakultas teknik</li>
    </ul>
  <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Akademik</div>
    <ul class="px-5 py-4 leading-loose list-decimal text-xl"> 
        <li class="">Akademik melakukan validasi terhadap berkas yang telah dikirim mahasiswa</li>
        <li class="">Akademik melakukan pembuatan surat</li>
        <li class="">Akademik berkewajiban membantu mahasiswa apabila melupakan password</li>
    </ul>
  <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Kepala Labolatorium</div>
    <ul class="px-5 py-4 leading-loose list-decimal text-xl"> 
        <li class="">Kepala lab melihat data peminjam alat di menu dashboard</li>
        <li class="">Kepala lab dapat menambahkan atau mengurangi data iventaris dari alat labolatorium</li>
        <li class="">Kepala lab melakukan konvirmasi alat apabila alat telah kembali</li>
        <li class="">Kepala lab mengirimkan surat keterangan konvirmasi labolatorium, apabila mahasiswa tersebut telah mengembalikan seluruh alat yang di pinjam</li>
    </ul>
  <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Login</div>
    <ul class="px-5 py-4 leading-loose list-decimal text-xl"> 
        <li class="">Login dilakukan hanya oleh civitas akademika</li>
        <li class="">Password awal di tetapkan oleh akademik</li>
        <li class="">Proses pengubahan password dilakukan di bagian akademik</li>
        <li class="">Apabila civitas akademika lupa password maka pengurusan dapat mengajukannya secara offline ke bagian akademik</li>
        <li class="">Harap menginputkan data username dan password dengan benar</li>
    </ul>
  <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Pengajuan Peminjaman Alat</div>
    <ul class="px-5 py-4 leading-loose list-decimal text-xl"> 
        <li class="">Masuk ke menu login</li>
        <li class="">Pilih 'Peminjaman'</li>
        <li class="">Download Tamplate Berkas Surat Pengajuan Peminjaman Alat</li>
        <li class="">Pilih Labolatorium</li>
        <li class="">Select Pilihan Alat di Menu Iventaris Alat</li>
        <li class="">Dokumen yang Diupload Maksimal 500 Kb bertipe file PDF</li>
    </ul>
  <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Bebas Lab</div>
    <ul class="px-5 py-4 leading-loose list-decimal text-xl"> 
        <li class="">Masuk ke menu login</li>
        <li class="">Pilih 'Bebas Lab'</li>
        <li class="">Mahasiswa Menginputkan Data (*mahasiswa dapat mengurus apabila checklist dari kepala labolatorium telah terferivikasi)</li>
        <li class="">Mahasiswa memantau perkembangan surat bebas labolatorium</li>
        <li class="">Unduh Surat Bebas Labolatorium</li>
        <li class="">Dokumen yang Diupload Maksimal 500 Kb bertipe file PDF</li>
    </ul>
</div>
@endsection