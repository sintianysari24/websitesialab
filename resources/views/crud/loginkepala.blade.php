@extends('_layouts.induk2')
@section('induk2')

<div class="flex justify-center item-center w-full">
<div class="w-full max-w-xs item center justify-center">
    <div class="text-center py-2">
    <a class="font-bold text-2xl text-blue-800">KEPALA LABOLATORIUM</a>
    </div>
    <form class="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4">
      <div class="mb-2">
        <label class="block text-lg text-center text-gray-700 text-sm font-bold mb-2" for="nipakademik">
          NIP
        </label>
        <input class="shadow appearance-none border text-center rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="nipakademik" type="text" placeholder="NIP">
      </div>
      <div class="mb-6">
        <label class="block text-center text-lg text-gray-700 text-sm font-bold mb-2" for="password">
          Password
        </label>
        <input class="shadow appearance-none border border-red-500 text-center rounded-full w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="******************">
        <p class="text-red-500 text-xs italic">Please choose a password.</p>
      </div>
      <div class="flex items-center justify-center">
        <a href="{{route('lab.labdashboard')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full focus:outline-none focus:shadow-outline">
          MASUK
        </a>
      </div>
    </form>
    <p class="text-center text-gray-500 text-xs">
      &copy;2020 Acme Corp. All rights reserved.
    </p>
  </div>
</div>
@endsection