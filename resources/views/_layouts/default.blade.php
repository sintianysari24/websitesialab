<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Portal Peminjaman & Pengembalian Alat Lab</title>

  {{-- untuk menggunkan tailwind --}}
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="/css/app.css">
</head>

<body class="bg-gray-300 text-gray-700">
  <div class="flex flex-col h-screen">
    {{-- navbar --}}
    <div class="flex px-2 py-2 bg-blue-800 justify-left items-left text-white text-left">
      <div class="flex-initial px-2">
        <img class="h-5/12 w-20" src="/img/Logo-Universitas-Mulawarman-UNMUL.png" alt="">
      </div>
      <div class="flex-initial self-center">
      <a class="font-bold font-normal text-base">SISTEM PEMINJAMAN/PENGEMBALIAN ALAT LAB</a><br>
      <a class="font-bold font-normal text-base">FAKULTAS TEKNIK</a><br>
      <p class="font-bold font-normal text-base">UNIVERSITAS MULAWARMAN</p>
      </div>

    </div>
  
    <div class="flex flex-grow overflow-hidden">
      <div id="hid" class="bg-gray-800 flex flex-col items-center space-y-7">
        <i onclick="w3_open()" href="" class="m-3 material-icons cursor-pointer text-white">menu</i>
      </div>
    <div class="flex flex-grow">
      <div id="mySidebar" class="bg-gray-800 flex flex-col items-center space-y-7 hidden">  
        <a onclick="w3_close()" class="flex font-bold px-24 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center material-icons cursor-pointer">arrow_back</a>
        <div class="flex flex-col justify-center ">
          <div class="flex justify-center">
            <img class="h-5/12 w-16 m-3 justify-center item center" src="/img/user.png" alt="">
          </div>
          <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-800 hover:bg-yellow-600 text-white justify-center" href="{{route('mahasiswa.usermhs')}}">USER</a>
        </div>
        <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center text-center" href="{{route('mahasiswa.dashboardmhs')}}">DASBOARD</a>
        <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-800 hover:bg-yellow-600 text-white justify-center text-center" href="{{route('mahasiswa.mhspinjam')}}">PEMINJAMAN</a>
        <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center text-center" href="{{route('mahasiswa.mhskembali')}}">PENGEMBALIAN</a>

        <a onclick="active2()" id="drop3" class="flex font-bold px-10 py-2 rounded-lx bg-gray-800 hover:bg-yellow-600 text-white justify-center" href="#">BEBAS LAB</a>
          <div id="drop4" class="hidden w-full bg-yellow-400 flex flex-col items-center space-y-7">
            <a href="{{route('mahasiswa.mhscheck')}}" class="font-bold w-full py-2 rounded-lx text-blue-800 hover:text-blue-700 text-center text-white">CHECKLIST LAB</a>
            <a href="{{route('mahasiswa.mhsinput')}}" class="font-bold w-full py-2 rounded-lx text-blue-800 hover:text-blue-700 text-center text-white">INPUT DATA</a>
            <a href="{{route('mahasiswa.mhsupdate')}}" class="font-bold w-full px-10 py-2 rounded-lx text-blue-800 hover:text-blue-700 text-center text-white">UPDATE</a>

          </div>    
          <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center" href="{{route('crud.dashboard')}}">LOGOUT</a>
      </div>
        <div class="overflow-y-auto p-10 w-full h-full">
          @yield('content')
        </div>    
    </div>
  </div>
</body>
<script>
  function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("hid").style.display = "none";
  }
  
  function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("hid").style.display = "block";
  }

  var drop3 = false;
  function active2(){
    drop3 = true;
    var drop4 = document.getElementById('drop4');
    drop4.classList.remove('hidden')
  }
  function hide(){
    var drop2 = document.getElementById('drop4');
    drop2.classList.add('hidden')
  }
</script>
</html>
