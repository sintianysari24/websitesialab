<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Portal Peminjaman & Pengembalian Alat Lab</title>

  {{-- untuk menggunkan tailwind --}}
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body class="bg-gray-300 text-gray-700">
  <div class="flex flex-col h-screen">
    {{-- navbar --}}
    <div class="flex px-2 py-2 bg-blue-800 justify-left items-left text-white text-left">
      <div class="flex-initial px-2">
        <img class="h-5/12 w-20" src="/img/Logo-Universitas-Mulawarman-UNMUL.png" alt="">
      </div>
      <div class="flex-initial self-center">
      <a class="font-bold font-normal text-base">SISTEM PEMINJAMAN/PENGEMBALIAN ALAT LAB</a><br>
      <a class="font-bold font-normal text-base">FAKULTAS TEKNIK</a><br>
      <p class="font-bold font-normal text-base">UNIVERSITAS MULAWARMAN</p>
      </div>

    </div>
  
    <div class="flex flex-grow overflow-hidden">
      <div id="hid" class="bg-gray-800 flex flex-col items-center space-y-7">
        <i onclick="w3_open()" href="" class="m-3 material-icons cursor-pointer text-white">menu</i>
      </div>
    <div class="flex flex-grow">
      <div id="mySidebar" class="bg-gray-800 flex flex-col items-center space-y-7 hidden">  
        <a onclick="w3_close()" class="flex font-bold px-20 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center material-icons cursor-pointer">arrow_back</a>
        <div class="flex flex-col justify-center ">
          <div class="flex justify-center">
            <img class="h-5/12 w-16 m-3 justify-center item center" src="/img/user.png" alt="">
          </div>
          <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-800 hover:bg-yellow-600 text-white justify-center" href="{{route('akademik.akauser')}}">USER</a>
        </div>
        <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center" href="{{route('akademik.akadashboard')}}">DASBOARD</a>
        <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-800 hover:bg-yellow-600 text-white justify-center" href="{{route('akademik.akabebas')}}">BEBAS LAB</a>
  
          <a class="flex font-bold px-10 py-2 rounded-lx bg-gray-700 hover:bg-yellow-600 text-white justify-center" href="{{route('crud.dashboard')}}">LOGOUT</a>
      </div>
    </div>
  {{-- tempat untuk lokasi child/anak --}}
        <div class="overflow-y-auto p-10 w-full h-full">
          @yield('akademik')
        </div>    
    </div>
  </div>
</body>
<script>
  function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("hid").style.display = "none";
  }
  
  function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("hid").style.display = "block";
  }
  </script>
</html>
