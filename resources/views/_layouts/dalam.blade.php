<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Portal Peminjaman & Pengembalian Alat Lab</title>

  {{-- untuk menggunkan tailwind --}}
  <link rel="stylesheet" href="/css/app.css">
</head>
<body class="bg-gray-300 text-gray-700">
    <div class="flex flex-col h-screen">
          {{-- navbar --}}
        <div class="flex px-2 py-2 bg-blue-800 justify-left items-left text-white text-left">
            <div class="flex-initial px-2">
              <img class="h-5/12 w-20" src="/img/Logo-Universitas-Mulawarman-UNMUL.png" alt="">
            </div>
            <div class="flex-initial self-center">
            <a class="font-bold font-normal text-base">SISTEM PEMINJAMAN/PENGEMBALIAN ALAT LAB</a><br>
            <a class="font-bold font-normal text-base">FAKULTAS TEKNIK</a><br>
            <p class="font-bold font-normal text-base">UNIVERSITAS MULAWARMAN</p>
            </div>
        </div>
        <div class="flex bg-yellow-500 item-center">
            <div class="text-blue-900 bg-yellow-500 px-4 py-1">
                    <p Class="font-bold font-normal text-center text-xl m-1">Update Pergerakan Surat Bebas Labolatorium</p>
            </div>
        </div>
        <div class="overflow-y-auto p-10 w-full h-full">
            @yield('dalam')
        </div>  
    </div>
</body>
</html>