@extends('_layouts.default')
@section('content')
<div class="space-y-6">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Proses Registrasi Data
        </p>
    </div>
    <div class="row-span-1 col-span-2 bg-white rounded-lg">
        <form class="px-4 py-2">
            <div class="flex flex-wrap ">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-nama">
                      Nama
                    </label>
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="nama" >
                </div>
            </div>
            <div class="flex flex-wrap">
              <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="NIM">
                  NIM
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="nim">
              </div>
            </div>
            <div class="flex flex-wrap ">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="Tanggallahir">
                      Tanggal Lahir
                    </label>
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="tanggallahir" placeholder="DD/MM/YYYY">
                </div>
            </div>
            <div class="flex flex-wrap">
                <div class="w-full px-3">
                  <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="alamat">
                    Alamat
                  </label>
                  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="alamat">
                </div>
            </div>
            <div class="flex flex-wrap">
              <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="phone">
                  No. HP
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="phone" placeholder="08xx-xxxx-xxxx">
              </div>
            </div>
            <div class="flex flex-wrap">
                <div class="w-full px-3">
                  <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="tanggalpengajuan">
                    Tanggal Pengajuan
                  </label>
                  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="tanggalpengajuan">
                </div>
            </div>
            <div class="inline-block relative w-64 flex flex-wrap px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Keperluan
                </label>
                <select class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                  <option>Pilih Keperluan</option>
                  <option>Surat Bukti Bebas Lab</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg class="fill-current h-5 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                </div>
              </div>
              <div class="w-full px-3 py-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  FC Slip UKT Terbaru
                </label>
                <input type="file" name="filename" class="py-3">
                <a href="" class="mt-2 bg-yellow-600 hover:bg-yellow-700 text-white font-bold py-1 px-4 rounded-full focus:outline-none focus:shadow-outline">
                  Upload
                </a>
              </div>
              <div class="w-full px-3 py-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Surat Bukti Konvirmasi Bebas Labolatorium
                </label>
                <input type="file" name="filename" class="py-3">
                <a href="" class="mt-2 bg-yellow-600 hover:bg-yellow-800 text-white font-bold py-1 px-4 rounded-full focus:outline-none focus:shadow-outline">
                  Upload
                </a>
              </div>
        </form>
    </div>
    <div class="">
        <div class="">
        <a href="" class="bg-yellow-600 px-10 py-1 hover:bg-yellow-800 hover:font-bold text-white text-xl rounded-full">Simpan</a>
        <a href="" class="bg-yellow-600 px-12 py-1 hover:bg-green-800 hover:font-bold text-white text-xl rounded-full">Kirim</a>
        </div>
    </div>
</div>
@endsection
