@extends('_layouts.default')
@section('content')
<div class="space-y-2">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Update Pergerakan Surat Bebas Labolatorium
        </p>
    </div>
    <div class="row-span-1 col-span-2 bg-white rounded-lg py-4">
        <div class="text-center">
            <div class="text-3xl m-b-md">
            Pergerakan Surat<br>
            </div>
        </div>
        <div class="md:flex justify-center px-16">
            <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center">
                <div class="flex-1  px-16">
                    <img class="h-5/12 w-20 m-3" src="/img/berkas.png" alt="">
                </div>
                <div class="flex-1 px-16">
                    <p class="text-xl">Proses<br>Pegistrasi</P>
                </div>
            </div>
            <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center">
                <div class="flex-1 px-16">
                    <img class="h-5/12 w-20 m-3" src="/img/terima.png" alt="">
                </div>
                <div class="flex-1 px-16">
                    <p class="text-xl">Data<br>Terkirim</P>
                </div>
            </div>
            <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center">
                <div class="flex-1 px-16">
                    <img class="h-5/12 w-20 m-3" src="/img/centang.png" alt="">
                </div>
                <div class="flex-1 px-16">
                    <p class="text-xl">Proses<br>Pengesahan</P>
                </div>
            </div>
            <div class="md:flex-shrink-0 mt-6 flex flex-col justify-center item-center text-center self-center">
                <div class="flex-1 px-16">
                    <img class="h-5/12 w-20 m-3" src="/img/unduh.png" alt="">
                </div>
                <div class="flex-1 px-16">
                    <p class="text-xl">Unduh<br>Surat</P>
                </div>
            </div>
        </div>
        <div class=" text-center"><br>
            <a href="" class="py-1 px-16 bg-yellow-600 hover:bg-yellow-700 rounded-full text-white text-2xl">Unduh</a>
        </div>
    </div>
</div>
@endsection