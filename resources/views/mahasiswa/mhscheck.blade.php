@extends('_layouts.default')
@section('content')
<div class="space-y-2">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Check List Setiap Kepala Labolatorium
        </p>
    </div>
    <div class="py-4">
        <p class="">Note: Unduh data sebagai bukti telah terkonvirmasi oleh setiap kepala lab gabungkan dan upload setiap bukti di menu input data</p>
        <table class="rounded-lg w-full mt-4">
            <tr class="bg-gray-100 bg-white">
                <th class="px-2 bg-yellow-400">No</th>
                <th class="px-2 bg-yellow-400">Nama Labolatorium</th>
                <th class="px-2 bg-yellow-400">Unduh Surat Bukti Data Terkonvirmasi</th>
                <th class="px-2 bg-yellow-400">Status</th>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">1</td>
                <td class="py-2 px-2">Labolatorium Elektro</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">2</td>
                <td class="py-2 px-2">Labolatorium Kimia</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">3</td>
                <td class="py-2 px-2">Labolatorium Lingkungan</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">4</td>
                <td class="py-2 px-2">Labolatorium Pertambangan</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">5</td>
                <td class="py-2 px-2">Labolatorium Giologi</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">6</td>
                <td class="py-2 px-2">Labolatorium Sipil</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">7</td>
                <td class="py-2 px-2">Labolatorium Industri</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
            <tr class="bg-white hover:bg-yellow-200 text-center">
                <td class="py-2 px-2">8</td>
                <td class="py-2 px-2">Labolatorium Komputasi</td>
                <td class="py-2 px-2">
                    <a href="#" class="rounded-full bg-green-600 hover:bg-green-700 text-white px-10 py-1">Unduh</a>                    
                </td>
                <td class="py-2 px-2">Belum Terkonvirmasi</td>      
            </tr>
        </table> 
    </div>  
</div>
@endsection
