@extends('_layouts.default')
@section('content')
<div class="space-y-2">
  <div class="w-full bg-yellow-500">
      <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
          Proses Peminjaman Alat
      </p>
  </div>
  <div class="flex-col  px-2 py-2 bg-yellow-400 justify-left items-left text-left">
    <div class="flex">
        <div class="flex-initial px-2">
        <img class="h-5/12 w-8" src="/img/iconpdf.png" alt="">
        </div>
        <div class="flex-initial self-center text-xs">
        <a class="font-normal text-base text-black">Tamplate Surat Pengajuan Peminjaman Alat Labolatorium</a>
        </div>
        <div class="flex-initial self-center px-4">
            <a href=""class="font-normal text-base text-white bg-blue-900 px-4 py-1 rounded-full hover:bg-blue-800">Unduh</a>
        </div>
    </div>
    <div class="flex">
        <div class="w-full px-3 py-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xl">
                Surat Pengajuan Peminjaman Alat Labolatorium
            </label>
            <input type="file" name="filename"class="py-3">
            <a href="" class="mt-3 bg-blue-900 hover:bg-blue-800 text-white font-bold py-1 px-4 rounded-full focus:outline-none focus:shadow-outline">
            Upload
            </a>
        </div>
    </div>
  </div>
</div>
<p class="block uppercase tracking-wide text-blue-900 text-base font-bold">Nama Labolatorium</p>
<div class="flex">
<div class="flex-initial">
<select class="block appearance-none bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
    <option>Pilih Labolatorium</option>
    <option>Labolatorium Teknik Elektro</option>
    <option>Labolatorium Teknik Komputasi</option>
    <option>Labolatorium Teknik Sipil</option>
    <option>Labolatorium Teknik Kimia</option>
    <option>Labolatorium Teknik Lingkungan</option>
    <option>Labolatorium Teknik Pertambangan</option>
    <option>Labolatorium Teknik Geologi</option>
    <option>Labolatorium Teknik Industri</option>
  </select>
</div>
<div class="flex flex-row-reverse w-full sapce-y-3">
<div>
    <a href="" class="px-5 py-2 bg-yellow-600 hover:bg-yellow-700 rounded-full text-white">Simpan</a>
</div>
<div>
    <a href=""  class="px-8 py-2 bg-yellow-600 hover:bg-yellow-700 rounded-full text-white">Kirim</a>
</div>
</div>
</div>
<div class="flex space-x-4">
    <div class="flex-1 overflow-y-auto h-40">
        <table class="rounded-lg w-full mt-2" >
            <thead class="bg-white">
            <tr class="bg-gray-100">
                <th class="border-b px-2 bg-yellow-400">No</th>
                <th colspan="2" class="border-b px-2 bg-yellow-400">Data Iventaris</th>          
            </tr>
            <tr class="text-center bg-gray-100 hover:bg-yellow-200">
                <td>1</td>
                <td class="col-span-1 px-auto">
                    <input type="checkbox" class="" id="multimetera">
                </td>
                <td class="col-span-1">
                    <label>Multimeter Analog</label>
                </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>2</td>
                <td>
                <input type="checkbox" class=""  id="power">
              </td>
              <td>
                <label>Power Supply</label>
              </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>3</td>
                <td>
                <input type="checkbox" class=""  id="osiloskop">
              </td>
              <td>
                <label>Osiloskop</label>
                </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>4</td>
                <td>
                <input type="checkbox" class=""  id="check">
              </td>
              <td>
                <label>Multimeter Digital</label>
                </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>5</td>
                <td>
                <input type="checkbox" class=""  id="kipas">
              </td>
              <td>
                <label>Kipas Angin</label>
              </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>6</td>
                <td>
                <input type="checkbox" class=""  id="cahaya">
              </td>
              <td>
                <label>KIT Kotak Cahaya</label>
              </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>7</td>
                <td>
                <input type="checkbox" class=""  id="resistor">
              </td>
              <td>
                <label>Resistor</label>
              </td>
            </tr>
            </thead>
        </table>       
    </div>
    <div class="flex-1">
        <table class="rounded-lg w-full mt-2">
            <thead class="bg-white">
            <tr class="bg-gray-100">
                <th class="border-b px-2 bg-yellow-400">No</th>
                <th class="border-b px-2 bg-yellow-400">Data Iventaris</th>          
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>1</td>
                <td>
                    Power Supply
                </td>
            </tr>
            <tr class="text-center px-auto bg-gray-100 hover:bg-yellow-200">
                <td>2</td>
                <td>
                    Multimeter Digital
                </td>
            </tr>
            </thead>
        </table>       
    </div>
</div>

@endsection