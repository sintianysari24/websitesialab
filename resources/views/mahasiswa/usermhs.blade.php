@extends('_layouts.default')
@section('content')
<div class="grid grid-rows-1 grid-flow-col gap-4 ">
    <div class="px-2 py-2 row-span-3 bg-yellow-500">
      <div class="flex justify-center">
        <img class="h-5/12 w-48 m-3" src="/img/user.png" alt="">
      </div>
        <p class="px-2 text-2xl font-boldtext-left justify-left">Data Diri</p>
        <p class="px-2 text-xl text-left justify-left">Nama</p>
        <p class="px-2 text-base text-left justify-left">Sintiany Dewi Ratna Sari</p>
        <p class="px-2 text-xl text-left justify-left">NIM</p>
        <p class="px-2 text-base text-left justify-left">1709075026</p>
        <p class="px-2 text-xl text-left justify-left">Program Studi</p>
        <p class="px-2 text-base text-left justify-left">Teknik Elektro</p>
    </div>
    <div class="row-span-1 col-span-2 bg-gray-800">
        <p class="px-2 text-3xl text-white text-center justify-center">Pengaturan Akun</p>
    </div>
    <div class="row-span-1 col-span-2 bg-white rounded-lg">
      <form class="px-4 py-2">
            <p class="px-2 text-xl text-left justify-left">Akun</p>
            <div class="flex flex-wrap ">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-email">
                      Email
                    </label>
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-email" placeholder="username@gmail.com">
                </div>
            </div>
            <div class="flex flex-wrap">
              <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                  Password
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" type="password" placeholder="******************">
              </div>
            </div>
          </form>
    </div>
    <div class="row-span-1 col-span-2 bg-white rounded-lg">
      <form class="px-4 py-2">
            <p class="px-2 text-xl text-left justify-left"><br>Umum</p>
            <div class="flex flex-wrap ">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-Nama">
                      Nama
                    </label>
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-Nama" placeholder="User">
                </div>
            </div>
            <div class="flex flex-wrap">
                <div class="w-full px-3">
                  <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-nim">
                    NIM
                  </label>
                  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-nim">
                </div>
            </div>
            <div class="flex flex-wrap">
              <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-phone">
                  No. HP
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-phone" placeholder="08xx-xxxx-xxxx">
              </div>
            </div>
            <div class="flex flex-wrap">
                <div class="w-full px-3">
                  <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-alamat">
                    Alamat
                  </label>
                  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-alamat">
                </div>
            </div>
            <div class="inline-block relative w-64 flex flex-wrap px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Program Studi
                  </label>
                <select class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                  <option >Pilih Prodi</option>
                  <option>Teknik Elektro</option>
                  <option>Teknik Komputasi</option>
                  <option>Teknik Sipil</option>
                  <option>Teknik Kimia</option>
                  <option>Teknik Lingkungan</option>
                  <option>Teknik Pertambangan</option>
                  <option>Teknik Geologi</option>
                  <option>Teknik Industri</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg class="fill-current h-5 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                </div>
              </div>
              <div class="w-full px-3 py-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Upload Foto
                </label>
                <input type="file" name="filename"class="py-3">
                <a href="" class="mt-3 bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-4 rounded-full focus:outline-none focus:shadow-outline">
                  Kirim
                </a>
              </div>
            <div class="flex px-3 py-4">
                <a href="" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                  Simpan
                </a>
            </div>
        </form>
    </div>
</div>
@endsection
