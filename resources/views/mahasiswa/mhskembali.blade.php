@extends('_layouts.default')
@section('content')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="space-y-2">
    <div class="w-full bg-yellow-500">
        <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
            Proses Pengembalian Alat
        </p>
    </div>
    <p class="w-full text-2xl py-1">Data Pengembalian Alat Labolatorium</p>
    <div class="">
        <table class="rounded-lg w-full mt-2">
            <thead class="bg-white">
            <tr class="bg-gray-100">
                <th class="border-b px-2 bg-yellow-400">No</th>
                <th class="border-b px-2 bg-yellow-400">Nama Labolatorium</th>   
                <th class="border-b px-2 bg-yellow-400">Nama Alat</th>    
                <th class="border-b px-2 bg-yellow-400">Status</th>       
            </tr>
            </thead>
            <tr class="bg-gray-100 hover:bg-yellow-200 text-center">
                <td class="border-b px-2">1</td>
                <td class="border-b px-2">Labolatorium Teknik Elektro</td>   
                <td class="border-b px-2">Multimeter Digital</td>    
                <td class="border-b px-2 flex justify-center">
                    <div class="flex-initial">
                    <i class="material-icons text-green-700 text-center px-1">check_circle</i>
                    </div>
                    <div class="flex-initial">
                     Kembali
                    </div>
                </td>       
            </tr>
            <tr class="bg-gray-100 hover:bg-yellow-200 text-center">
                <td class="border-b px-2">2</td>
                <td class="border-b px-2">Labolatorium Teknik Elektro</td>   
                <td class="border-b px-2">Power Supply</td>    
                <td class="border-b px-2 flex justify-center">
                    <div class="flex-initial">
                    <i class="material-icons text-red-700 text-center px-1">cancel</i>
                    </div>
                    <div class="flex-initial">
                     Belum Kembali
                    </div>
                </td>       
            </tr>
        </table>       
    </div>
</div>
@endsection