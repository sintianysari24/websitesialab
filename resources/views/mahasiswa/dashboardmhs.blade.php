@extends('_layouts.default')
@section('content')
<div class="space-y-2">
  <div class="w-full bg-yellow-500">
      <p class="w-full text-2xl px-2 py-1 text-blue-900 font-bold">
          Data Peminjaman dan Pengembalian Alat
      </p>
  </div>
</div>
  <div class="">
    <table class="rounded-lg w-full mt-4">
        <tr class="bg-gray-100">
          <th class="border-b px-2 bg-yellow-400">No</th>
          <th class="border-b px-2 bg-yellow-400">Tanggal Pinjam</th>
          <th class="border-b px-2 bg-yellow-400">Nama Labolatorium</th>
          <th class="border-b px-2 bg-yellow-400">Nama Alat</th>
          <th class="border-b px-2 bg-yellow-400">Tanggal Kembali</th>       
        </tr>
        <tr class="bg-gray-100 hover:bg-yellow-200 text-center ">
          <td class="border-b px-2">1</td>
          <td class="border-b px-2">09/10/2020</td>
          <td class="border-b px-2">Labolatorium Teknik Elektro</td>
          <td class="border-b px-2">Multimeter Digital</td>
          <td class="border-b px-2">-</td>       
        </tr>
        <tr class="bg-gray-100 hover:bg-yellow-200 text-center">
          <td class="border-b px-2">2</td>
          <td class="border-b px-2">09/10/2020</td>
          <td class="border-b px-2">Labolatorium Teknik Elektro</td>
          <td class="border-b px-2">Power Supply</td>
          <td class="border-b px-2">16/10/2020</td>       
        </tr>
    </table>
  </div>
@endsection
