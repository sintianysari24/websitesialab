@extends('_layouts.induk3')
@section('induk3')

<table class="rounded-lg w-full mt-4 m-3">
    <p class="text-3xl font-bold">Labolatorium Fakultas Teknik</p>
    <thead class="bg-white">
      <tr class="bg-gray-100 text-xl">
        <th class="border-b px-2 bg-yellow-400">No</th>
        <th class="border-b px-2 bg-yellow-400">Nama Labolatorium</th>   
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
          <td class="hover:bg-yellow-300 border-b">1</td>
          <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Elektro</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">2</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Industri</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">3</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Kimia</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">4</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Geologi</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">5</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Lingkungan</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">6</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Sipil</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">7</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Pertambangan</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">8</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Komputasi</td>          
      </tr>
    </thead>
</table><br>

<div>
    <p class="text-3xl font-bold text-center">Deskripsi</p><br>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Elektro</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            Program studi teknik elektro mempunyai Laboratorium Rekayasa Elektroi sebanyak 2 unit dengan luas 134 m2 dan mempunyai alat laboratorium yang digunakan sebagai alat peraga penunjang perkuliahan dan penelitian bagi mahasiswa dan dosen
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Industri</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Kimia</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Komputasi</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Sipil</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Pertambangan</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Geologi</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
    <div class="sticky top-0 bg-yellow-500 text-2xl px-2 text-blue-900 text-center font-bold rounded-full">Labolatorium Teknik Lingkungan</div>
        <p class="px-5 py-4 leading-loose text-xl">	
            NULLL
        </p>
  </div>
@endsection