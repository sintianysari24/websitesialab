@extends('_layouts.induk3')
@section('induk3')
<div class="flex">
    <div class="flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-orange-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-3xl text-left justify-left">Peminjaman Alat Labolatorium</P><br>
            <div class="text-left p-2">
                <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('crud.homelogin')}}">Select</a> 
            </div>
        </div>
        <div class="text-blue-900 text-center bg-white-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-base text-left text-justify">Fakultas Teknik memiliki berbagai macam fasilitas di dalamnya termasuk penyediaan labolatorium,
                selain itu mahasiswa dapat meminjam alat labolatorium guna keperluan penelitian</P>
        </div>
    </div>
    <div class="flex-1 text-white text-center bg-white bg-opacity-75 px-4 py-2 m-2 rounded-lg">
        <div class="text-white text-center bg-orange-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-3xl text-left justify-left">Bebas Labolatorium</P><br>
            <div class="text-left p-2">
                <a class="px-4 py-2 bg-blue-900 hover:bg-blue-800 text-white rounded-lg" href="{{route('crud.homelogin')}}">Select</a> 
            </div>
        </div>
        <div class="text-blue-900 text-center bg-white-600 px-4 py-2 m-2 rounded-lg">
            <P class="text-base text-left text-justify">Labolatorium fakultas teknik tentunya memberikan pelayanan pembuatan surat bebas labolatorium
                guna untuk syarat kelulusan
            </P>
        </div>
    </div>
</div>
@endsection