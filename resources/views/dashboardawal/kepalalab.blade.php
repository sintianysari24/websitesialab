@extends('_layouts.induk3')
@section('induk3')
<table class="rounded-lg w-full mt-4 m-3">
    <p class="text-3xl font-bold">Labolatorium Fakultas Teknik</p>
    <thead class="bg-white">
      <tr class="bg-gray-100 text-xl">
        <th class="border-b px-2 bg-yellow-400">No</th>
        <th class="border-b px-2 bg-yellow-400">Nama Labolatorium</th> 
        <th class="border-b px-2 bg-yellow-400">Kepala Labolatorium</th> 
        <th class="border-b px-2 bg-yellow-400">Kontak Person</th> 
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
          <td class="hover:bg-yellow-300 border-b">1</td>
          <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Elektro</td>        
          <td class="hover:bg-yellow-300 border-b">name</td>  
          <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>    
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">2</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Industri</td>  
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>           
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">3</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Kimia</td>  
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>           
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">4</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Geologi</td> 
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>            
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">5</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Lingkungan</td>  
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>           
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">6</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Sipil</td>   
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">7</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Pertambangan</td>   
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>          
      </tr>
      <tr class="hover:bg-yellow-200 border-b text-center text-xl">
        <td class="hover:bg-yellow-300 border-b">8</td>
        <td class="hover:bg-yellow-300 border-b">Labolatorium Teknik Komputasi</td>  
        <td class="hover:bg-yellow-300 border-b">name</td>  
        <td class="hover:bg-yellow-300 border-b">08xxxxxxxxxxx</td>           
      </tr>
    </thead>
</table><br>
@endsection