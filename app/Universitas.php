<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Universitas extends Model
{
  protected $table = 'universitas';

  protected $guarded = [
    'id', 'updated_at', 'created_at',
  ];
}
