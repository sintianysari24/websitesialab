<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MagangController extends Controller
{
    public function landing()
    {
        return view('welcome');
    }

    public function tugas($variabel, $data)
    {
        return view('testing', compact('variabel', 'data'));
    }
}
