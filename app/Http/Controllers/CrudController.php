<?php

namespace App\Http\Controllers;

use Request;
// model / table kita
use App\Sialab;

class CrudController extends Controller
{
//dashboard awal-----------------------------------
    public function labolatorium()
    {
      return view('dashboardawal.labolatorium');
    }
    public function pelayanan()
    {
      return view('dashboardawal.pelayanan');
    }
    public function kepalalab()
    {
      return view('dashboardawal.kepalalab');
    }
//home login---------------------------------------
      public function homelogin()
    {
      return view('crud.loginhome');
    }
    public function dashboard()
    {
      return view('crud.dashboard');
    }
    public function akademik()
    {
      return view('crud.loginakademik');
    }
    public function kepala()
    {
      return view('crud.loginkepala');
    }
    public function mahasiswa()
    {
      return view('crud.loginmahasiswa');
    }

//mahasiswa-----------------------------------------
    public function usermhs()
    {
      return view('mahasiswa.usermhs');
    }
    public function dashboardmhs()
    {
      return view('mahasiswa.dashboardmhs');
    }
    public function mhspinjam()
    {
      return view('mahasiswa.mhspinjam');
    }
    public function mhskembali()
    {
      return view('mahasiswa.mhskembali');
    }
    public function mhsinput()
    {
      return view('mahasiswa.mhsinput');
    }
    public function mhsupdate()
    {
      return view('mahasiswa.mhsupdate');
    }
    public function mhscheck()
    {
      return view('mahasiswa.mhscheck');
    }

//akademik------------------------------------------
    public function akauser()
    {
      return view('akademik.akauser');
    }
    public function akadashboard()
    {
      return view('akademik.akadashboard');
    }
    public function akabebas()
    {
      return view('akademik.akabebas');
    }
    public function akadidalam()
    {
      return view('akademik.didalam');
    }

// //labolatorium---------------------------------------
    public function labuser()
    {
      return view('lab.labuser');
    }  
    public function labdashboard()
    {
      // $sialab = Sialab::all();
      return view('lab.labdashboard');
    }    
    public function labkonvirmasi()
    {
      return view('lab.labkonvirmasi');
    }  
    public function labbebas()
    {
      return view('lab.labbebas');
    } 
    public function labtambah()
    {
      return view('lab.labtambah');
    } 
    public function help()
    {
      return view('crud.help');
    }
    public function mhslist()
    {
      return view('lab.list');
    }


// //crud controller--------------------------------------------
    public function create()
    {
      return view('lab.labtambah');
    }

//     public function store()
//     {
//       Request::validate([
//         'noAlat' => 'required',
//         'namaAlat' => 'required',
//         'jumlah' => 'required',
//         'jumlahPinjam' => 'required',
//         'kondisi' => 'required',
//         'tahunPembelian' => 'required',
//     ]);
//       $noAlat = Request::get('noAlat');
//       $namaAlat = Request::get('namaAlat');
//       $jumlah = Request::get('jumlah');
//       $jumlahPinjam = Request::get('jumlahPinjam');
//       $kondisi = Request::get('kondisi');
//       $tahunPembelian = Request::get('tahunPembelian');

//       Sialab::create([
//         'noAlat' => $noAlat,
//         'namaAlat' => $namaAlat,
//         'jumlah' => $jumlah,
//         'jumlahPinjam' => $jumlahPinjam,
//         'kondisi' => $kondisi,
//         'tahunPembelian' => $tahunPembelian,
//       ]);

//       return redirect()->route('lab.labdashboard');
//     }

//     public function edit($id)
//     {
//         $unv=Sialab::find($id);
//         return view('lab.labedit', compact('unv'));
//     }

//     public function update($id)
//     {
//       $noAlat = Request::get('noAlat');
//       $namaAlat = Request::get('namaAlat');
//       $jumlah = Request::get('jumlah');
//       $jumlahPinjam = Request::get('jumlahPinjam');
//       $kondisi = Request::get('kondisi');
//       $tahunPembelian = Request::get('tahunPembelian');

//       $unv =  Sialab::find($id);
//       $unv->update([
//         'noAlat' => $noAlat,
//         'namaAlat' => $namaAlat,
//         'jumlah' => $jumlah,
//         'jumlahPinjam' => $jumlahPinjam,
//         'kondisi' => $kondisi,
//         'tahunPembelian' => $tahunPembelian,
//       ]);

//       return redirect()->route('lab.labdashboard');
//     }

//     public function destroy($id)
//     {
//         $env = Sialab::find($id);
        
//         if ($env) {
//           $env->delete();
//           return redirect()->route('lab.labdashboard')
//           ->withSuccess('Data Berhasil Dihapus');
//         }else{
//           return redirect()-back('lab.labdashboard')
//           ->withError('Data Tudak Ditemukan');
//         }
//     }
}